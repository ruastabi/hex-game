(define listaempleados (list (list 1500 "juan perez") (list 3200 "pedro gonzalez") (list 2000 "maria rojas")))

(define nombres (lambda (n) (map cdr n)))
(nombres listaempleados)

(define factorprestacional 
  (
  lambda (n) 
    (
      map (lambda (m) (* 0.3 m) ) ( map car n )  
    ) 
  )  
) 
(factorprestacional listaempleados)

(define totalprestacional
  (
  lambda (n)
    (
      apply + (map (lambda (m) (* 0.3 m) ) ( map car n ))
    )
  )
)
(totalprestacional listaempleados)