package utils;

public enum Color {
	RED((byte)2), BLUE((byte)1);
	
	byte number;
    Color( byte number )
    {
        this.number = number;
    }
    public byte get() {
    	return this.number;
    }
}
