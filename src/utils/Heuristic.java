package utils;

import game.Box;

public enum Heuristic {
	FIRSTONE(
		new Box[] {
			new Box(-1,0),
			new Box(-1,1),
			new Box(0,1),
			new Box(0,-1),
			new Box(1,-1),
			new Box(1,0)
		}
	), 
	SECONDONE(
	new Box[] {
			new Box(-1,0),
			new Box(-1,-1),
			new Box(0,-1),
			new Box(0,1),
			new Box(1,1),
			new Box(1,0)
		}
	),
	NES_MM(
	new Box[] {
			new Box(1,1),
			new Box(-1,0),
			new Box(0,-1),
			new Box(-1,-1),
		}
	),
	NES_MC(
	new Box[] {
			new Box(1,0),
			new Box(0,-1),
			new Box(0,1),
			new Box(-1,-1),
			new Box(-1,1),
			new Box(-1,0),
		}
	),
	NES_MP(
	new Box[] {
			new Box(1,-1),
			new Box(-1,0),
			new Box(0,1),
			new Box(-1,1)
		}
	),
	NES_CM(
	new Box[] {
			new Box(0,1),
			new Box(-1,0),
			new Box(1,0),
			new Box(-1,-1),
			new Box(1,-1),
			new Box(0,-1)
		}
	),
	NES_CP(
	new Box[] {
			new Box(0,-1),
			new Box(1,0),
			new Box(-1,0),
			new Box(-1,1),
			new Box(1,1),
			new Box(0,1)
		}
	),
	NES_PM(
	new Box[] {
			new Box(-1,1),
			new Box(0,-1),
			new Box(1,0),
			new Box(1,-1)
		}
	),
	NES_PC(
	new Box[] {
			new Box(-1,0),
			new Box(0,-1),
			new Box(0,1),
			new Box(1,-1),
			new Box(1,1),
			new Box(1,0),
		}
	),
	NES_PP(
	new Box[] {
			new Box(-1,-1),
			new Box(1,0),
			new Box(0,1),
			new Box(1,1)
		}
	);
	
	private Box[] heuristic;
	Heuristic(Box[] data){
		this.heuristic=data;
	}
	
	public Box[] getH() { 
		return this.heuristic;
	}
	
	public boolean contains(Box box) {
		for (int i = 0; i < heuristic.length; i++) {
			if(heuristic[i].getI() == box.getI() && heuristic[i].getJ() == box.getJ()) {
				return true;
			}
		}
		return false;
	}
}
