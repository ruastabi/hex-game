package utils;

import game.Board;

public class Boards {
	private Board board1;
	private Board board2;
	private Board board3;
	private Board board4;
	private Board board5;
	private Board board6;
	
	public Board getBoard1() {
		return board1;
	}
	public Board getBoard2() {
		return board2;
	}
	public Board getBoard3() {
		return board3;
	}
	public Board getBoard4() {
		return board4;
	}
	public Board getBoard5() {
		return board5;
	}
	public Board getBoard6() {
		return board6;
	}
	
	public Boards() {
		super();
		board1();
		board2();
		board3();
		board4();
		board5();
		board6();
	}
	
	private void board1() {
		board1= new Board();
		board1.setBox(0, 3, 2);
		board1.setBox(2, 4, 2);
		board1.setBox(4, 5, 2);
		board1.setBox(6, 5, 1);
		board1.setBox(6, 6, 1);
		board1.setBox(6, 7, 1);
	}
	
	private void board2() {
		board2= new Board();
		board2.setBox(0, 3, 2);
		board2.setBox(2, 4, 2);
		board2.setBox(4, 5, 2);
		board2.setBox(6, 5, 1);
		board2.setBox(6, 6, 1);
		board2.setBox(6, 7, 1);
	}
	
	private void board3() {
		board3= new Board();
		board3.setBox(0, 3, 2);
		board3.setBox(2, 4, 2);
		board3.setBox(4, 5, 2);
		board3.setBox(6, 5, 1);
		board3.setBox(6, 6, 1);
		board3.setBox(6, 7, 1);
	}
	
	private void board4() {
		board4= new Board();
		board4.setBox(0, 3, 2);
		board4.setBox(2, 4, 2);
		board4.setBox(4, 5, 2);
		board4.setBox(6, 5, 1);
		board4.setBox(6, 6, 1);
		board4.setBox(6, 7, 1);
	}
	
	private void board5() {
		board5= new Board();
		board5.setBox(0, 3, 2);
		board5.setBox(2, 4, 2);
		board5.setBox(4, 5, 2);
		board5.setBox(6, 5, 1);
		board5.setBox(6, 6, 1);
		board5.setBox(6, 7, 1);
	}
	
	private void board6() {
		board6= new Board();
		board6.setBox(0, 3, 2);
		board6.setBox(2, 4, 2);
		board6.setBox(4, 5, 2);
		board6.setBox(6, 5, 1);
		board6.setBox(6, 6, 1);
		board6.setBox(6, 7, 1);
	}
}
