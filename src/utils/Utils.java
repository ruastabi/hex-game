package utils;

import game.Box;

public class Utils {

	public Utils() {
		
		// TODO Auto-generated constructor stub
	}
	
	public static int unit(int n) {
		return (n==0)?0:(n<0)?-1:1;
	}
	
	public static int unit(double n) {
		return (n==0)?0:(n<0)?-1:1;
	}

	public static Box[] neighbors_ab(Box a, Box b){
		Box[] neighbors = new Box[4];
		int i=Utils.unit(b.getI()-a.getI());
		int j=Utils.unit(b.getJ()-a.getJ());
		switch (i) {
		case -1:
			switch (j) {
			case -1:
				neighbors = Heuristic.NES_MM.getH();
				break;
			case 0:
				neighbors = Heuristic.NES_MC.getH();
				break;
			case 1:
				neighbors = Heuristic.NES_MP.getH();
				break;
			}
			break;
		case 0:
			switch (j) {
			case -1:
				neighbors = Heuristic.NES_CM.getH();
				break;
			case 1:
				neighbors = Heuristic.NES_CP.getH();
					break;
			}
			break;
		case 1:
			switch (j) {
			case -1:
				neighbors = Heuristic.NES_PM.getH();
				break;
			case 0:
				neighbors = Heuristic.NES_PC.getH();
				break;
			case 1:
				neighbors = Heuristic.NES_PP.getH();
				break;
			}
			break;
		}
		return neighbors;
	}
}
