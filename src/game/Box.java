package game;

public class Box {
	private int i;
	private int j;
	
	public Box(int i, int j) {
		super();
		this.i = i;
		this.j = j;
	}
	
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}
	public int getJ() {
		return j;
	}
	public void setJ(int j) {
		this.j = j;
	}
	
	public boolean equals(Box o) {
		if (o.getI() == this.i && o.getJ()==this.j)
			return true;
		else
			return false;
	}
	
	public String toString() {
		return Integer.toString(i)+"-"+Integer.toString(j);
	}
	
	public Box add(Box a) {
		return new Box(this.i+a.getI(),this.j+a.getJ());
	}
}
