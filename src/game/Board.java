package game;

public class Board {
	private int board[][];
	
	public Board(int[][] board) {
		super();
		board= new int[11][11];
		for (int i=0;i<11;i++) {
			for (int j=0;j<11;j++) {
				board[i][j]=0;
			}
		}
		this.board = board;
	}
	
	public Board() {
		super();
		this.board= new int[11][11];
	}

	public int[][] getBoard() {
		return board;
	}

	public void setBoard(int[][] board) {
		this.board = board;
	}
	
	public int getState(int i,int j) {
		if(i<0 || i>10 || j<0 || j>10) {
			return Game.INF;
		}
		return board[i][j];
	}
	
	public int getState(Box box) {
		int i=box.getI();
		int j=box.getJ();
		if(i<0 || i>10 || j<0 || j>10) {
			return Game.INF;
		}
		return board[i][j];
	}
	
	public void setBox(int i,int j, int data) {
		board[i][j]=data;
	}
}
