package game;

public class Move {
	public Move(byte i, byte j) {
		super();
		this.i = i;
		this.j = j;
	}
	private byte i;
	private byte j;
	public byte getI() {
		return i;
	}
	public void setI(byte i) {
		this.i = i;
	}
	public byte getJ() {
		return j;
	}
	public void setJ(byte j) {
		this.j = j;
	}
}
