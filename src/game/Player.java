package game;

import java.lang.Math;
import java.util.HashMap;
import java.util.Stack;
import java.util.logging.Logger;

import utils.Heuristic;
import utils.Utils;

/**
 * @author Valentina
 *
 */

public class Player {
	
	public Player(Logger log, Board board, Heuristic h, byte mycolor, byte hercolor, int[][] waysB, int[][] waysR) {
		super();
		this.log = log;
		this.board = board;
		this.h = h;
		this.mycolor = mycolor;
		this.hercolor = hercolor;
		this.waysB = waysB;
		this.waysR = waysR;
	}

	private Logger log;
	
	private Board board;
	private Heuristic h = Heuristic.SECONDONE;
	private static int INF= Game.INF;
//	El color rojo equivale al camino vertical y el azul al horizontal.
//	Blue=1, Red=2
//	Defino dos colores para facilitar la lectura y la escritura en momentos de comparaci�n
	private byte mycolor;
	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public void setBoard(int[][] board) {
		this.board = new Board(board);
	}
	public byte getMycolor() {
		return mycolor;
	}

	public void setMycolor(byte mycolor) {
		this.mycolor = mycolor;
	}

	public byte getHercolor() {
		return hercolor;
	}

	public void setHercolor(byte hercolor) {
		this.hercolor = hercolor;
	}

	private byte hercolor;
	
//	Matrices cach� con las rutas m�s cortas. (No siempre son las m�s inteligentes (casi nunca) )
	private int[][] waysB;
	private int[][] waysR;
	
	
	/**
	 * Constructor de la clase jugador
	 * @param mycolor
	 * @param board
	 */
	public Player(byte mycolor, Board board) {
		this.board=board;
		this.mycolor=mycolor;
		if(mycolor==1) {
			this.hercolor=2;
		}
		if(mycolor==2) {
			this.hercolor=1;
		}
		this.waysB = new int[11+2][11+1];
		this.waysR = new int[11+1][11+2];

		this.log = Logger.getGlobal();
	}
	
	/**
	 * Método inicial del jugador.
	 * @return
	 */
	public Move play() {
		if( whoiswinning() == mycolor) {
			log.finest("Jugador ganando");
			log.finest("Construyendo ruta");
		}
		else {
			log.finest("Jugador perdiendo o empatado");
			log.finest("Cortando ruta");
		}
		Move move=null;
		return move;	
	}
	
	/**
	 * Calcula el jugador que est� m�s pr�ximo a ganar, es decir, el que necesita menos movimientos para conectar un camino final.
	 * @return
	 */
	public int whoiswinning() {
		shortestwayB();
		shortestwayR();
		int minb=INF;
		int minr=INF;
		for (int i=1; i<12; i++) {
			if (minr > waysR[11][i]) {
				minr = waysR[11][i];
			}
			if (minb > waysB[i][11]) {
				minb = waysB[i][11];
			}
		}
		log.fine("La ruta más corta del rojo es "+minr);
		log.fine("La ruta más corta del azul es "+minb);
		return minb<minr? 1: minb>minr? 2 :0;
	}
	
//	La ruta m�s corta con memoria din�mica O(121).
//	No deber�amos usar este algoritmo porque encuentra las rutas m�s cortas bordeando las jugadas del otro color,
//	movimiento demasiado arriesgado. Sin embargo, encuentra literalmente la ruta m�s corta.
	/**
	 * Calcula la matriz de rutas m�s cortas hasta todos los valores, iniciando desde la primera fila.
	 * @return
	 */
	public int shortestwayR() {
		for(int i=0;i<12;i++) {
			for(int j=0;j<13;j++){
				waysR[i][j]=-1;
				if(j==0 || j==12) waysR[i][j]=INF;
				if(i==0) waysR[i][j]=0;
			}
		}
		
		for (int i=1;i<12;i++) {
			for (int j=1;j<12;j++) {
				if(waysR[i][j]==-1) {
					waysR[i][j]=shortestwayR(i,j);
				}
			}			
		}
		return 0;
	}
	
	/**
	 * Calcula la ruta m�s corta para la posici�n i, j agregando las subrutas m�s cortas en la cach� waysR.
	 * @param i
	 * @param j
	 * @return
	 */
	public int shortestwayR(int i, int j) {
		int sum=1;
		if (board.getState(i-1, j-1) == 1) {
			return INF;
		}
		if (board.getState(i-1, j-1) == 2) {
			sum=0;
		}
		int a=waysR[i-1][j];
		int b=waysR[i-1][j-1];
		int c=waysR[i][j+1];
		if (c==-1) {
			c=shortestwayR(i,j+1);
		}
		
		return Math.min(Math.min(a,b),c)+sum;
	}
	
	/**
	 * Imprime la matriz de rutas más cortas para el color rojo.
	 */
	public void showWaysR() {
		for (int i=0;i<12;i++) {
			for (int j=0;j<13;j++) {
				System.out.print(waysR[i][j]+" ");
			}
			System.out.println();
		}
	}
	
//	La ruta más corta con memoria dinámica O(121).
//	No deberíamos usar este algoritmo porque encuentra las rutas más cortas bordeando las jugadas del otro color,
//	movimiento demasiado arriesgado. Sin embargo, encuentra literalmente la ruta más corta.
	/**
	 * Calcula la matriz de rutas m�s cortas hasta todos los valores, iniciando desde la primera fila.
	 * @return
	 */
	public int shortestwayB() {
		for(int i=0;i<13;i++) {
			for(int j=0;j<12;j++){
				waysB[i][j]=-1;
				if(i==0 || i==12) waysB[i][j]=INF;
				if(j==0) waysB[i][j]=0;
			}
		}
		
		for (int j=1;j<12;j++) {
			for (int i=1;i<12;i++) {
				if(waysB[i][j]==-1) {
					waysB[i][j]=shortestwayB(i,j);
				}
			}			
		}
		return 0;
	}
	
	/**
	 * Calcula la ruta más corta para la posición i, j agregando las subrutas más cortas en la cach� waysB.
	 * @param i
	 * @param j
	 * @return
	 */
	public int shortestwayB(int i, int j) {
		int sum=1;
		if (board.getState(i-1, j-1) == 2) {
			return INF;
		}
		if (board.getState(i-1, j-1) == 1) {
			sum=0;
		}
		int a=waysB[i][j-1];
		int b=waysB[i-1][j-1];
		int c=waysB[i+1][j];
		if (c==-1) {
			c=shortestwayB(i+1,j);
		}
		
		return Math.min(Math.min(a,b),c)+sum;
	}
	
	
	public int dijkstra(Box a, Box b) {
		int l=1;
		int[][] matrix = new int[12][12];
		
//		Se inicializa la matriz 
		for (int i = 0; i < matrix.length; i++) 
			for (int j = 0; j < matrix.length; j++)
				matrix[i][j]=-1;
		
		matrix[a.getI()][a.getJ()]= board.getState(a)==mycolor? 0:1;
		
		while (l<12) {
			for(int i=0;i<=l;i++) {
				Box[] boxes = { a.add(new Box(-l,i)),  a.add(new Box(l,i)),  a.add(new Box(-(l-i),l)),  a.add(new Box(l-i,l)) };
				int min=INF;
				for (Box bo : boxes) {
					if(board.getState(bo) == hercolor && board.getState(bo) == INF) {
						matrix[bo.getI()][bo.getJ()]=INF;
					}
					else {
						for (Box box : h.getH()) {
							Box neighbor = bo.add(box);
							if(board.getState(neighbor) != INF) {
								if(matrix[neighbor.getI()][neighbor.getJ()] != -1) {
									if(matrix[neighbor.getI()][neighbor.getJ()] < min) {
										min=matrix[neighbor.getI()][neighbor.getJ()];
									}
								}								
							}
						}
						if(board.getState(bo) == mycolor) {
							matrix[bo.getI()][bo.getJ()]=min;
						}
						if(board.getState(bo) == 0) {
							matrix[bo.getI()][bo.getJ()]=min+1;
						}						
					}
				}
			}
			l++;
		}
		
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				System.out.print(matrix[i][j]+"  ");
			}
			System.out.println();
		}
		
		return 0;
	}
	
	/**
	 * Dadas dos casillas, constituidas cada uno por un arreglo de enteros en donde la posición 0
	 * es el número de fila y la posición 1 el número de columna; se retorna la ruta más corta. 
	 * @param a
	 * @param b
	 * @return
	 */
	public int greedy_dijkstra(Box a, Box b) {
		Boolean found= false;
		Stack<Box> stack = new Stack<Box>();
		HashMap<String, Boolean> checked = new HashMap<String, Boolean>();
		HashMap<String, Integer> cost = new HashMap<String, Integer>();
		stack.add(a);
		cost.put(a.toString(),board.getState(a)==mycolor?0:1);
		while(!found){
			Box actual=stack.pop();
			System.out.println("Actualmente en "+actual.toString());
			checked.put(actual.toString(), true);
			Box[] neighbors = Utils.neighbors_ab(actual, b);
			int cont = 0;
			for (Box box : neighbors) {
				if(this.h.contains(box) ) {
					Box nbox = actual.add(box);
					if(board.getState(nbox) != hercolor && board.getState(nbox)!= INF) {
						System.out.println("Evaluando "+nbox.toString());
						int c = (board.getState(nbox) == mycolor? 0:1);
						if(checked.containsKey(nbox.toString())) {
							cost.put(nbox.toString(),Math.min(cost.get(nbox.toString()), cost.get(actual.toString()) +c) );
						}
						else {
							System.out.println("Agregando a "+nbox.toString());
							cost.put(nbox.toString(), cost.get(actual.toString())+c);
							stack.add(nbox);
						}											
					}
					else {
						cont++;
					}
					found = nbox.equals(b);
					if (found) return cost.get(b.toString());
				}
			}	
		}
		return -1;
	}
	
	/**
	 * Imprime la matriz de rutas más cortas para el color azul.
	 */
	public void showWaysB() {
		for (int i=0;i<13;i++) {
			for (int j=0;j<12;j++) {
				System.out.print(waysB[i][j]+" ");
			}
			System.out.println();
		}
	}
}

 