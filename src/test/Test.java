/**
 * 
 */
package test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import game.Player;
import utils.Boards;
import utils.Color;

/**
 * @author vale
 *
 */
public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			FileHandler logs = new FileHandler("logs/"+LocalDateTime.now().hashCode()+".log");
			
			Logger logger = Logger.getGlobal();
			logger.addHandler(logs);
			logger.setLevel(Level.FINE);
			SimpleFormatter formatter = new SimpleFormatter();  
	        logs.setFormatter(formatter);  
			
			Boards boards= new Boards();
			Player player= new Player(Color.BLUE.get(), boards.getBoard1());
			player.whoiswinning();
			
			player.setBoard(boards.getBoard2());
//			Test greedy dijkstra
/*			logger.fine("Test Greedy Dijkstra de 0,0 a 5,5");
			player.greedy_dijkstra(new Box(0,0), new Box(5,5));
			logger.fine("Test Greedy Dijkstra de 1,3 a 2,1");
			player.greedy_dijkstra(new Box(1,3), new Box(2,1));
*/			
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
