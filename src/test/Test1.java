package test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import game.Box;
import game.Player;
import utils.Boards;
import utils.Color;

public class Test1 {

	public Test1() {
		// TODO Auto-generated constructor stub
	}
	
//	Test de whoiswinning y dijkstra
	public static void main(String[] args) {
		try {
			FileHandler logs = new FileHandler("logs/"+LocalDateTime.now().hashCode()+".log");
			
			Logger logger = Logger.getGlobal();
			logger.addHandler(logs);
			logger.setLevel(Level.FINE);
			SimpleFormatter formatter = new SimpleFormatter();  
	        logs.setFormatter(formatter);  
			
			Boards boards= new Boards();
			
			Player player1= new Player(Color.BLUE.get(), boards.getBoard1());
			player1.play();
			
			Player player2= new Player(Color.RED.get(), boards.getBoard1());
			player2.play();
			
			int n;
//			n = player1.greedy_dijkstra(new Box(6,4), new Box(6,8));
//			System.out.println("El costo es "+n);
//			
//			n = player2.greedy_dijkstra(new Box(6,4), new Box(6,8));
//			System.out.println("El costo es "+n);
//			
//			n = player1.greedy_dijkstra(new Box(5,6), new Box(7,6));
//			System.out.println("El costo es "+n);
//			
//			n = player2.greedy_dijkstra(new Box(5,6), new Box(7,6));
//			System.out.println("El costo es "+n);
			
			n = player1.dijkstra(new Box(6,4), new Box(6,8));
			
			
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
